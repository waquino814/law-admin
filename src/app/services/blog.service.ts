import { Injectable } from '@angular/core';
import { actions } from '../stores/blog.store';
import { Observable, Subscription } from 'rxjs/Rx';
import { Store } from '@ngrx/store';
import { Post } from '../data-objects/post';
import { Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class BlogService {
  private postsUrl = '../assets/mock/posts.json';
  private blogState$: Observable<Post[]>;
  private blogStateSubscription: Subscription;

  constructor(private http: Http, private store: Store<Post[]>) {
    this.blogState$ = store.select('blog');
  }

  loadData() {
    this.http.get(this.postsUrl)
      .toPromise()
      .then(response => {
        this.store.dispatch({ type: actions.LOAD, payload: response.json() as Post[] });
      })
      .catch(this.handleError);
  }
  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }
}
