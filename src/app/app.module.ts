import { PreviewPostComponent } from './blog/preview-post/preview-post.component';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppRoutingModule } from './app.routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { BlogComponent } from './blog/blog.component';
import { CKEditorModule } from 'ng2-ckeditor';
import { NewPostComponent } from './blog/new-post/new-post.component';
import {BlogService} from './services/blog.service';
import { StoreModule } from '@ngrx/store';
import { BlogReducer } from './stores/blog.store';

//prime uses
import {ButtonModule} from 'primeng/primeng';
import {ToolbarModule} from 'primeng/primeng';
import {InputTextModule} from 'primeng/primeng';
import {DataTableModule, SharedModule} from 'primeng/primeng';
import {EditorModule} from 'primeng/primeng';
import {TooltipModule} from 'primeng/primeng';
import {MessagesModule} from 'primeng/primeng';
import {DialogModule} from 'primeng/primeng';
import {OverlayPanelModule} from 'primeng/primeng';

//end of prime
import { BreadCrumbComponent } from './common/bread-crumb/bread-crumb.component';

@NgModule({
  declarations: [
    AppComponent,
    BreadCrumbComponent,
    HomeComponent,
    NotFoundComponent,
    BlogComponent,
    NewPostComponent,
    PreviewPostComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    ButtonModule,
    EditorModule,
    OverlayPanelModule,
    MessagesModule,
    TooltipModule,
    DataTableModule,
    DialogModule,
    SharedModule,
    InputTextModule,
    ToolbarModule,
    FormsModule,
    AppRoutingModule,
    CKEditorModule,
    StoreModule.provideStore({blog: BlogReducer}),
    HttpModule
  ],
  providers: [BlogService],
  bootstrap: [AppComponent]
})
export class AppModule { }
