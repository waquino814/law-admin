import { Component } from '@angular/core';
import {BlogService} from './services/blog.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app works!';
  constructor(private blogService: BlogService) {
    this.loadData();
  }
  loadData() {
    this.blogService.loadData();
  }
}
