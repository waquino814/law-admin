import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {AppComponent }          from './app.component';

import { HomeComponent } from './home/home.component';
import { BlogComponent } from './blog/blog.component';
import { NewPostComponent } from './blog/new-post/new-post.component';
import { NotFoundComponent } from './not-found/not-found.component';


const routes: Routes = [
  { path: 'home',  component: HomeComponent },
  { path: 'blog', component: BlogComponent},
  { path: 'blog/new-post', component: NewPostComponent},
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: '**',  component: NotFoundComponent },
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
