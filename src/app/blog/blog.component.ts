import { Component, OnInit } from '@angular/core';
import { Router} from '@angular/router';
import { Post } from '../data-objects/post';
import { actions } from '../stores/blog.store';
import { Observable, Subscription } from 'rxjs/Rx';
import { Action, Store } from '@ngrx/store';

@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.css']
})
export class BlogComponent implements OnInit {
  private blogState$: Observable<Post[]>;
  private blogStateSubscription: Subscription;
  private selectedPosts = [];
  private showConfirmDelete = false;
  private msgConfirmDelete = '';
  private postToPreview: Post;
  hideme = {};
  constructor(private router: Router, private store: Store<Post[]>) {
    this.blogState$ = store.select('blog');
  }

  ngOnInit() {
  }
  onNewPost() {
    this.router.navigate(['/blog/new-post']);
  }
  hideShow(id: string ) {
    this.hideme[id] = !this.hideme[id];
  }
  showDeletePosts() {
    if (this.selectedPosts.length > 0) {
      this.showConfirmDelete = true;
      this.msgConfirmDelete = 'You are about to delete ' + this.selectedPosts.length + ' posts. Are you sure?';
    }
  }
  showDeletePost(post) {
    this.showConfirmDelete = true;
    this.msgConfirmDelete = 'You are about to delete this post. Are you sure?';
    this.selectedPosts = [post];
  }
  deleteSelectedPosts() {
    for (let post of this.selectedPosts) {
      this.store.dispatch({type: actions.DELETE, payload:post});
    }
  }
  editPost(post) {
    this.router.navigate(['/blog/new-post', {id: post.id}]);
  }
  previewPost(post) {
    this.postToPreview = post;
    console.log('Preview Post', post);
  }
}
