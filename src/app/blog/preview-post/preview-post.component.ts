import { Input, Output, EventEmitter } from '@angular/core';
import { Post } from '../../data-objects/post';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-preview-post',
  templateUrl: './preview-post.component.html',
  styleUrls: ['./preview-post.component.css']
})
export class PreviewPostComponent implements OnInit {

  @Input()
  private post: Post;
  @Input()
  private showing = true;
  @Output()
  private onHide: EventEmitter<any> = new EventEmitter();
  constructor() { }

  ngOnInit() {
  }
  hid() {
    this.onHide.emit();
  }


}
