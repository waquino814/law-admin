import { create } from 'domain';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';
import { Post } from '../../data-objects/post';
import { actions } from '../../stores/blog.store';
import { Observable, Subscription } from 'rxjs/Rx';
import { Store } from '@ngrx/store';
@Component({
  selector: 'app-new-post',
  templateUrl: './new-post.component.html',
  styleUrls: ['./new-post.component.css']
})
export class NewPostComponent implements OnInit {
  private breadCrumbMenu = [{ route: '/home', name: 'Home' }, { route: '/blog', name: 'Publications' }, { active: true, name: 'New Post' }];
  private blogState$: Observable<Post[]>;
  private blogStateSubscription: Subscription;
  private posts: Post[];
  private post: Post;
  private postToPreview: Post;
  private newPost = true;

  constructor(private router: Router, private route: ActivatedRoute, private store: Store<Post[]>) {
    this.blogState$ = store.select('blog');
    this.blogStateSubscription = this.blogState$.subscribe(state => this.posts = state);
  }
  ngOnInit() {
    if (this.route.snapshot.params['id']) {
      let id = this.route.snapshot.params['id'];
      this.post = this.posts.find(item => item.id === id);
      this.newPost = false;
    }
  }

  onSubmit(f: NgForm) {
    if (this.newPost) {
      this.createPost(f);
    } else {
      this.updatePost(f);
    }
  }
  updatePost(f: NgForm) {
    this.post.title = f.value.title;
    this.post.content = f.value.content;
    this.store.dispatch({ type: actions.UPDATE, payload: this.post });
    f.reset();
    this.router.navigate(['/blog']);
  }
  createPost(f: NgForm) {
    let id = 'post-' + this.posts.length + 1;
    let imag = 'tempimg.jpg';
    let createdDate = new Date().toLocaleDateString();
    let post = new Post(id, imag, f.value.content, createdDate);
    post.title = f.value.title;
    this.store.dispatch({ type: actions.ADD, payload: post });
    f.reset();
    this.router.navigate(['/blog']);
  }
  onPreview(f: NgForm) {
    let createdDate = new Date().toLocaleDateString();
    let post = new Post('', '', f.value.content, createdDate);
    post.title = f.value.title;
    this.postToPreview = post;
  }
  onCancel() {
    this.router.navigate(['/blog']);
  }

}
