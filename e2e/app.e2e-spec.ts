import { LawAdminPage } from './app.po';

describe('law-admin App', function() {
  let page: LawAdminPage;

  beforeEach(() => {
    page = new LawAdminPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
